<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token'];
    protected $hidden = ['create_at','update_at'];

    public fuction post()
    {

        return $this->belongsto('App\post');
    }

    public fuction comments()
    {

        return $this->belongsto('App\comment');
    }

    ];
}
