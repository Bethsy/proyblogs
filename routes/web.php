<?php
use App\Models\publishes\publish;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('user/', function () {
    return user::get();

Route::get('publishes/', function () {
    return publish::get();
});

Route::get('publishes/{publishes_id}', function ($publish_name) {
    return publish::where ("name","=",$publish_name)->first();
});

Route::get('photos/', function () {
    return photo::get();

Route::get('post/', function () {
    return post::get();

Route::get('categories/', function () {
    return categori::get();

Route::get('category_language/', function () {
    return category_languag::get();

Route::get('language_post/', function () {
    return language_post::get();


    


