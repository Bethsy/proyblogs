<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriLanguajeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categori_language', function (Blueprint $table) 
        {

            $table->integer('category_id')->unsigned();
            $table->foreign('category_id')
            ->references('id')
            ->on('categories'); 
            $table->integer('languages_id')->unsigned();
            $table->foreign('languages_id')
            ->references('id')
            ->on('languages');
            $table->string('label',255);
            $table->string('slug',255);
            $table->text('description');
            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categori_language');
    }
}
