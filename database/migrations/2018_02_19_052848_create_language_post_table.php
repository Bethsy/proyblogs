<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLanguagePostTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('language_post', function (Blueprint $table) {
            
            $table->integer('posts_id')->unsigned();
            $table->foreign('posts_id')
            ->references('id')
            ->on('posts'); 
            $table->integer('languages_id')->unsigned();
            $table->foreign('languages_id')
            ->references('id')
            ->on('languages');
             $table->string('title',255);
            $table->string('slug',255);
            $table->text('content');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('language_post');
    }
}
